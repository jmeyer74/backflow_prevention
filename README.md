# Storm Drain Backflow Prevetion

- Royal Oak Review Begin:2023-10-02
- Royal Oak Review End: 2023-10-06

7/12 Approvals Complete

## Installation 

![installed 2023-11-07](img/installed.jpg)
## Concerns:

1. *Does this require a permit?*
I called the Denver permitting office, since this does not go inside a house, they are not responsible or interested in issuing a permit and or inspection for this.  
The other entity that might be interested is Dever wastewater.  I'm in the process of getting in touch with them.

2. *Chad*
Emailed Content:

Thank you for your detailed critique and insights regarding the flooding issue in our common area. I understand your concerns and would like to provide some additional information that might help clarify the proposed solution.

You're correct in noting that any check-valve will introduce some resistance to the outward flow of water. However, it's essential to understand the specifics of the valve we're considering. The valve is designed to open at a water pressure equivalent to a 6" water depth. This means that the valve will activate when the water in the pit reaches a depth greater than 6". It's important to note that this doesn't imply a constant 6" of water in the pit. Due to the design of the valve, water can bypass it around the rubber cone and the seal against the pipe wall.  Also this valve is rated for 1800 gpm per their specs.

The primary objective of this valve is to mitigate the massive backflow of water from the city pipe when the storm drain system is overwhelmed. To put things in perspective, a 6" water depth across the entire parking area and garages equates to approximately 28,000 gallons of water. This volume can be visualized as 100'x75'x.5' = 3,750 cubic feet. Based on the information I received, this volume of water appeared rapidly and drained within about 10 minutes on both occasions it was observed.

Given these details, my assessment is that our primary challenge is to counteract the sudden surge of backflow long enough to prevent future floods. By installing the check-valve, we aim to provide a buffer against these surges, allowing our drainage system to cope more effectively with large volumes of water.

I genuinely appreciate your feedback and the opportunity to clarify the proposed solution. I'm open to further discussions and any other insights or suggestions you might have.

## Release of Liability

This Release of Liability Agreement (“Agreement”) is entered into by and between the undersigned property owners (“Owners”) and [justin meyer dba Rocketscience] (“Installer”), collectively referred to as the “Parties.”

#### 1. Purpose of Agreement:
The purpose of this Agreement is to install a backflow prevention device (“Device”) to prevent flooding in the driveways and garages of the Owners’ properties located at 1919 w 46th Ave.

#### 2. Installation and Maintenance:
Installer agrees to install the Device and validate its operation periodically. The total cost of installation is $5800.  The installer agrees to cover the cost of this installation.

#### 3. Release and Waiver of Liability:
Owners hereby release, waive, discharge, and covenant not to sue Installer, its officers, agents, or employees, from any and all liability, claims, demands, actions, and causes of action whatsoever arising out of or related to any loss, damage, or injury, including death, that may be sustained by Owners, or any of the property belonging to Owners, whether caused by the negligence of Installer or otherwise, relating to the installation, maintenance, or operation of the Device.

#### 4. Indemnification:
Owners agree to indemnify and hold harmless Installer from any loss, liability, damage, or costs, including court costs and attorney's fees, that they may incur due to the installation, maintenance, or operation of the Device, whether caused by negligence of Installer or otherwise.

#### 5. Acknowledgment of Understanding:
Owners acknowledge that they have read this Agreement, understand its terms, and are executing this Agreement voluntarily and without any duress or undue influence. Owners understand that they are giving up substantial rights, including their right to sue. Owners acknowledge that they intend this release to be a complete and unconditional release of all liability to the greatest extent allowed by law.

#### 6. Governing Law:
This Agreement shall be governed by and construed in accordance with the laws of Colorado.

#### 7. Entire Agreement:
This Agreement contains the entire agreement between the Parties and supersedes any prior written or oral agreements between them concerning the subject matter of this Agreement.

## WaPro WS280


### Check Valve

The [WS280](https://wapro.com/en-gb/products/wastop-inline-check-valve?gclid=CjwKCAjw3oqoBhAjEiwA_UaLtvrl1o7p9vCmcwfPwgmTUGw3eOE1Jfti2Phb3SmA7hCbkyAnoKe2gRoC9swQAvD_BwE) is specifically designed for the purpose of backflow prevention.

![valve cartridge](img/valve.png)

### Installation Location

![drain](/img/drain.png)

The installation of the check valve is designed to be completely reversible, allowing for its removal at any time if it proves ineffective in preventing flooding of the driveway. This installation does not result in any permanent modifications to the drain, ensuring that the original state of the drain can be restored without any complications or structural changes.


#### Contributions

If this is approved and you are able to contribute my accounts are listed below.  Please dont sent me any money until Royal Oak has an approval from everyone recorded.

venmo: @turbohoje 303-834-7418

zelle: 303-669-3787

paypal: @turbohoje
